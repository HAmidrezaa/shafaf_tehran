(function($) {
	var tabs =  $(".tabs li a"); 
	tabs.on("click",function() {
		var content = this.hash.replace('/','');
		tabs.removeClass("active");
		$(this).addClass("active");
		$("#content").find('div').hide();
		$(content).fadeIn(100);
	});
	
	})(jQuery);

(function() {
	var btn = $("#one #submit");
	btn.on("click", function(){
		var data = {}
		data["نام کارفرما"]= document.getElementById("employer_name").value;
		data["نام پیمانکار"]= document.getElementById("contractor_name").value;
		data["از تاریخ"]= document.getElementById("contract_date__gte").value;
		data["تا تاریخ"]= document.getElementById("contract_date__lte").value;
		data["از مبلغ"]= document.getElementById("contract_price__gte").value;
		data["تا مبلغ"]= document.getElementById("contract_price__lte").value;
		$.ajax({
			url: 'http://localhost:8000/api/FilterCostData/',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
				if( response['result'].length ==0){
					alert("هیچ قراردادی وجود ندارد!")
				}
				else{
					for (var i=0; i<response['result'].length; i++) {
						if(response['result'][i]['نام مدیرعامل'] == null){
							response['result'][i]['نام مدیرعامل'] = " ";
						}
						var row = $('<tr><td>'+response['result'][i]['موضوع قرارداد']+'</td>'+'<td>'+response['result'][i]['نام کارفرما']+'</td>'+'<td>'+response['result'][i]['نام پیمانکار']+'<td>'+ response['result'][i]['نام مدیرعامل']+'</td>'+'</td></tr>');
						$('#first_table').append(row);
					}
					btn.removeClass("form");
					$(this).addClass("first_table");
					$("#one").find('form').hide();
					$("#first_table").fadeIn(200);
					$("#content").addClass("#button1");
					$("#button1").fadeIn(1000)
					console.log("show btn")
					$('form').trigger("reset");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' - ' + errorThrown);
			}
		});
	});
})(jQuery);

(function(){
	var btn = $("#one #output");
	btn.on("click", function(){
		$.ajax({
			url: 'http://localhost:8000/api/GetCostOutput/',
			type: 'GET',
			success: function() {
				window.location = 'http://localhost:8000/api/GetCostOutput/';
			}
		});
	});
})(jQuery);

(function(){
	var btn = $("#two #output");
	btn.on("click", function(){
		$.ajax({
			url: 'http://localhost:8000/api/GetIncomeOutput/',
			type: 'GET',
			success: function() {
				window.location = 'http://localhost:8000/api/GetIncomeOutput/';
			}
		});
	});
})(jQuery);

(function() {
	var btn = $("#two #submit");
	btn.on("click", function(){
		var data = {}
		data["نام کارفرما"]= document.getElementById("employer_name2").value;
		data["نام پیمانکار"]= document.getElementById("contractor_name2").value;
		data["از تاریخ"]= document.getElementById("contract_date__gte2").value;
		data["تا تاریخ"]= document.getElementById("contract_date__lte2").value;
		data["از مبلغ"]= document.getElementById("contract_price__gte2").value;
		data["تا مبلغ"]= document.getElementById("contract_price__lte2").value;
		$.ajax({
			url: 'http://localhost:8000/api/FilterIncomeData/',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
				if( response['result'].length ==0){
					alert("هیچ قراردادی وجود ندارد!")
				}
				else{
					for (var i=0; i<response['result'].length; i++) {
						if(response['result'][i]['نام مدیرعامل'] == null){
							response['result'][i]['نام مدیرعامل'] = " ";
						}
						var row = $('<tr><td>'+response['result'][i]['موضوع قرارداد']+'</td>'+'<td>'+response['result'][i]['نام کارفرما']+'</td>'+'<td>'+response['result'][i]['نام پیمانکار']+'<td>'+ response['result'][i]['نام مدیرعامل']+'</td>'+'</td></tr>');
						$('#second_table').append(row);
					}
					btn.removeClass("form");
					$(this).addClass("second_table");
					$("#two").find('form').hide();
					$("#second_table").fadeIn(200);
					$(this).addClass("#button2");
					$("#button2").fadeIn(200);
					$('form').trigger("reset");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' - ' + errorThrown);
			}
		});
	});
})(jQuery);


(function(){
	var btn = $("#three #output");
	btn.on("click", function(){
		$.ajax({
			url: 'http://localhost:8000/api/GetParticipatoryOutput/',
			type: 'GET',
			success: function() {
				window.location = 'http://localhost:8000/api/GetParticipatoryOutput/';
			}
		});
	});
})(jQuery);

(function() {
	var btn = $("#three #submit");
	btn.on("click", function(){
		var data = {}
		data["آدرس محل انجام کار"]= document.getElementById("address").value;
		data["سرمایه گذار"]= document.getElementById("contractor_name3").value;
		data["از تاریخ"]= document.getElementById("contract_date__gte3").value;
		data["تا تاریخ"]= document.getElementById("contract_date__lte3").value;
		data["از مبلغ"]= document.getElementById("contract_price__gte3").value;
		data["تا مبلغ"]= document.getElementById("contract_price__lte3").value;
		console.log(data)
		$.ajax({
			url: 'http://localhost:8000/api/FilterParticipatoryData/',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
				if( response['result'].length ==0){
					alert("هیچ قراردادی وجود ندارد!")
				}
				else{
					for (var i=0; i<response['result'].length; i++) {
						if(response['result'][i]['ذینفع'] == null){
							response['result'][i]['ذینفع'] = " ";
						}
						var row = $('<tr><td>'+response['result'][i]['موضوع قرارداد']+'</td>'+'<td>'+response['result'][i]['نام مدیرعامل']+'</td>'+'<td>'+response['result'][i]['سرمایه گذار']+'<td>'+response['result'][i]['ذینفع']+'</td>'+'</td>+</tr>');
						$('#third_table').append(row);
					}
					btn.removeClass("form");
					$(this).addClass("third_table");
					$("#three").find('form').hide();
					$("#third_table").fadeIn(200);
					$(this).addClass("ٍ#button3");
					$("#button3").fadeIn(200);
					$('form').trigger("reset");
					
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' - ' + errorThrown);
			}
		});
	});
})(jQuery);

(function(){
	var btn = $("#one #button1");
	btn.on("click", function(){
		document.getElementById("first_table").remove()
		$("form").fadeIn(200)
		document.getElementById("button1").remove()
		window.location.reload();
		

	});
})(jQuery);

(function(){
	var btn = $("#two #button2");
	btn.on("click", function(){
		document.getElementById("second_table").remove()
		$("form").fadeIn(200)
		document.getElementById("button2").remove()
	});
})(jQuery);

(function(){
	var btn = $("#three #button3");
	btn.on("click", function(){
		console.log("btn3")
		document.getElementById("third_table").remove()
		$("form").fadeIn(200)
		document.getElementById("button3").remove()
	});
})(jQuery);

