from django.contrib import admin
from django.urls    import path
from api.views      import AllIncomeData, AllParticipatoryData, AllCostData, GetData, GetIncomeOutput, GetCostOutput, GetParticipatoryData,FilterParticipatoryData, FilterCostData, FilterIncomeData, GetParticipatoryOutput

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/GetData/', GetData),
    path('api/GetParticipatoryData/', GetParticipatoryData),
    path('api/GetCostOutput/', GetCostOutput),
    path('api/GetIncomeOutput/', GetIncomeOutput),
    path('api/GetParticipatoryOutput/', GetParticipatoryOutput),
    path('api/FilterParticipatoryData/', FilterParticipatoryData),
    path('api/FilterCostData/', FilterCostData),
    path('api/FilterIncomeData/', FilterIncomeData),
    path('api/AllIncomeData/', AllIncomeData),
    path('api/AllCostData/', AllCostData),
    path('api/AllparticipatoryData/', AllParticipatoryData)

]
