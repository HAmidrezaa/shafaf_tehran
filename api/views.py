import json 
import requests
import pandas as pd

from django.shortcuts import render
from django.http      import JsonResponse
from .celery          import celery_app
from api.models       import Data, ParticipatoryData
from django.http      import FileResponse

@celery_app.task
def GetData(request=None):
    try:
        data = Data.objects.all()
        if data:
            data.delete()

        url =  "http://31.24.238.142/api/shafaf/loadcontracts/1"
        response = requests.get(url)
        datas = json.loads(response.text)

        for data in datas:
            
            data['LegalNationalId'] = int(str(data.get('LegalNationalId') or "-").replace("-","0"))
            data['LegalEconomicCode'] = int(str(data.get('LegalEconomicCode') or "-").replace("-","0"))
            data['ContractDate'] = str(data.get('ContractDate')).replace("/","-")
            data['ContractDate'] = str(data.get('ContractDate')).replace('          ',"1200-01-01")
            
            if data['StartDate'] != None:
                data['StartDate'] = str(data.get('StartDate')).replace("/","-")
            if data['FinishDate'] != None:
                data['FinishDate'] = str(data.get('FinishDate')).replace("/","-")
            if data['RealBirthDate'] != None:
                data['RealBirthDate'] = str(data.get('RealBirthDate')).replace("/","-")
                data['RealBirthDate'] = str(data.get('RealBirthDate')).replace('          ',"1200-01-01")


            data = Data.objects.create(
                type = 'C',
                VAT  = data['HasVat'],
                cash = int(data['CashPrice']),
                start_date = data['StartDate'],
                end_date   = data['FinishDate'],
                contract_date = data['ContractDate'],
                non_cash = int(data['NoneCashPrice']),
                contract_subject = data['RequestTitle'],
                contractor_name  = data['ContractorName'],
                supervisor_name  = data['SuperVisorName'],
                contract_duration = data['TermContract'],
                employer_name = data['OrganizationTitle'],
                contract_price = int(data['CurrentPrice']),
                contractor_birthdate = data['RealBirthDate'],
                manager_name = data['ContractorConectorName'],
                leave_reason = data['LeaveFormalityDescription'],
                contractor_signer = data['RequesterConectorName'],
                contractor_national_code = data['LegalNationalId'],
                commisstion_member = data['CommisstionMemberNames'],
                contractor_economic_code = data['LegalEconomicCode'],
                contract_attachmentes = data['ContractAttachmentDes'],
                contractor_selection  = data['ContractTypeDescription'],
                winniner_determination_method = data['TenderMethodTypeDescription'],
                contract_termination_information = data['ContractTerminationDes'],
            )

        url = "http://31.24.238.142/api/shafaf/loadcontracts/0"
        response = requests.get(url)
        datas = json.loads(response.text)

        for data in datas:
            
            data['LegalNationalId'] = int(str(data.get('LegalNationalId') or "-").replace("-","0"))
            data['LegalEconomicCode'] = int(str(data.get('LegalEconomicCode') or "-").replace("-","0"))
            data['ContractDate'] = str(data.get('ContractDate')).replace("/","-")
            data['ContractDate'] = str(data.get('ContractDate')).replace('          ',"1200-01-01")
            
            if data['StartDate'] != None:
                data['StartDate'] = str(data.get('StartDate')).replace("/","-")
            if data['FinishDate'] != None:
                data['FinishDate'] = str(data.get('FinishDate')).replace("/","-")
            if data['RealBirthDate'] != None:
                data['RealBirthDate'] = str(data.get('RealBirthDate')).replace("/","-")
                data['RealBirthDate'] = str(data.get('RealBirthDate')).replace('          ',"1200-01-01")


            data = Data.objects.create(
                type = 'I',
                VAT  = data['HasVat'],
                cash = int(data['CashPrice']),
                start_date = data['StartDate'],
                end_date   = data['FinishDate'],
                contract_date = data['ContractDate'],
                non_cash = int(data['NoneCashPrice']),
                contract_subject = data['RequestTitle'],
                contractor_name  = data['ContractorName'],
                supervisor_name  = data['SuperVisorName'],
                contract_duration = data['TermContract'],
                employer_name = data['OrganizationTitle'],
                contract_price = int(data['CurrentPrice']),
                contractor_birthdate = data['RealBirthDate'],
                manager_name = data['ContractorConectorName'],
                leave_reason = data['LeaveFormalityDescription'],
                contractor_signer = data['RequesterConectorName'],
                contractor_national_code = data['LegalNationalId'],
                commisstion_member = data['CommisstionMemberNames'],
                contractor_economic_code = data['LegalEconomicCode'],
                contract_attachmentes = data['ContractAttachmentDes'],
                contractor_selection  = data['ContractTypeDescription'],
                winniner_determination_method = data['TenderMethodTypeDescription'],
                contract_termination_information = data['ContractTerminationDes'],
            )
        
    except Exception as e:
        raise Exception ("connection error")

    return JsonResponse({'isOk': True}, status=200)
    

@celery_app.task
def GetParticipatoryData(request=None):
    data = ParticipatoryData.objects.all()
    if data:
        data.delete()

    url = "http://31.24.238.142/api/shafaf/loadcontracts/0"
    response = requests.get(url)
    datas = json.loads(response.text)
    
    for data in datas:
        data['LegalNationalId'] = int(str(data.get('LegalNationalId') or "-").replace("-","0"))
        data['LegalEconomicCode'] = int(str(data.get('LegalEconomicCode') or "-").replace("-","0"))
        data['ContractDate'] = str(data.get('ContractDate')).replace("/","-")
        data['ContractDate'] = str(data.get('ContractDate')).replace('          ',"1200-01-01")
        if data['StartDate'] != None:
                data['StartDate'] = str(data.get('StartDate')).replace("/","-")
        if data['FinishDate'] != None:
            data['FinishDate'] = str(data.get('FinishDate')).replace("/","-")

        data = ParticipatoryData.objects.create(
            municipality_description = data['PartnerShipMunicipalityDescription'],
            investor_description     = data['PartnerShipInvestorDescription'],
            contractor_signer        = data['RequesterConectorName'],
            contractor_national_code = data['LegalNationalId'],
            contractor_economic_code = data['LegalEconomicCode'],
            contract_duration = data['TermContract'], 
            contract_subject  = data['RequestTitle'], 
            contractor_name   = data['ContractorName'], 
            beneficiary_name  = data['BeneficiaryName'], 
            project_subject   = data['ProjectName'], 
            manager_name      = data['ContractorConectorName'], 
            contract_type     = data['ContractTypeDescription'], 
            use_type          = data['UseType'], 
            trustee_name      = data['TrusteeName'],
            contract_price    = data['CurrentPrice'], 
            contract_date     = data['ContractDate'], 
            start_date = data['StartDate'], 
            end_date   = data['FinishDate'], 
            address    = data['WorkSpaceAddress']
        )

    return JsonResponse({'isOk': True}, status=200)
    


def GetCostOutput(request):
    datas = Data.objects.filter(type="C")
    excel_data = [ {
        "موضوع قرارداد": data.contract_subject,
        "نام پیمانکار": data.contractor_name,
        "نام کارفرما": data.employer_name,
        "مبلغ نهایی قرارداد": data.contract_price,
        "تاریخ انعقاد قرارداد": data.contract_date,
        "نام مدیرعامل": data.manager_name,
        "مبلغ غیرنقد": data.non_cash,
        "مبلغ نقد": data.cash,
        " اطلاعات فسخ قرارداد": data.contract_termination_information,
        " اطلاعات الحاقیه": data.contract_attachmentes,
        "مدت قرارداد": data.contract_duration,
        "روش انتخاب پیمانکار": data.contractor_selection,
        "شناسه ملی پیمانکار": data.contractor_national_code,
        "کد اقتصادی پیمانکار": data.contractor_economic_code,
        "تاریخ تولد پیمانکار": data.contractor_birthdate,
        "علت ترک تشریفات": data.leave_reason,
        "مالیات  بر ارزش افزوده دارد؟": data.VAT,
        "تاریخ شروع": data.start_date,
        "تاریخ پایان": data.end_date,
        " معاون": data.supervisor_name,
        "روش تعیین برنده": data.winniner_determination_method,
        "اعضای کمیسیون": data.commisstion_member,
        "امضا کننده‌ی قرارداد از طرف کارفرما ": data.contractor_signer
    } 
    for data in datas
    ]
    df = pd.DataFrame(data=excel_data)
    df.to_excel("قراردادهای هزینه‌ای.xlsx")

    response = FileResponse(open('قراردادهای هزینه‌ای.xlsx', 'rb'), as_attachment=True, filename='قراردادهای هزینه‌ای.xlsx')
    return response


def GetIncomeOutput(request):
    datas = Data.objects.filter(type="I")
    excel_data = [ {
        "موضوع قرارداد": data.contract_subject,
        "نام پیمانکار": data.contractor_name,
        "نام کارفرما": data.employer_name,
        "مبلغ نهایی قرارداد": data.contract_price,
        "تاریخ انعقاد قرارداد": data.contract_date,
        "نام مدیرعامل": data.manager_name,
        "مبلغ غیرنقد": data.non_cash,
        "مبلغ نقد": data.cash,
        " اطلاعات فسخ قرارداد": data.contract_termination_information,
        " اطلاعات الحاقیه": data.contract_attachmentes,
        "مدت قرارداد": data.contract_duration,
        "روش انتخاب پیمانکار": data.contractor_selection,
        "شناسه ملی پیمانکار": data.contractor_national_code,
        "کد اقتصادی پیمانکار": data.contractor_economic_code,
        "تاریخ تولد پیمانکار": data.contractor_birthdate,
        "علت ترک تشریفات": data.leave_reason,
        "مالیات  بر ارزش افزوده دارد؟": data.VAT,
        "تاریخ شروع": data.start_date,
        "تاریخ پایان": data.end_date,
        " معاون": data.supervisor_name,
        "روش تعیین برنده": data.winniner_determination_method,
        "اعضای کمیسیون": data.commisstion_member,
        "امضا کننده‌ی قرارداد از طرف کارفرما ": data.contractor_signer
    } 
    for data in datas
    ]
    df = pd.DataFrame(data=excel_data)
    df.to_excel("قراردادهای دارآمدی.xlsx")

    response = FileResponse(open('قراردادهای دارآمدی.xlsx', 'rb'), as_attachment=True, filename='قراردادهای دارآمدی.xlsx')
    return response


def GetParticipatoryOutput(request):
    datas = ParticipatoryData.objects.all()
    excel_data = [ {
        "موضوع قرارداد": data.contract_subject,
        "ذینفع": data.beneficiary_name,
        "سرمایه گذار": data.contractor_name,
        "نام پروژه": data.project_subject,
        "مبلغ نهایی قرارداد": data.contract_price,
        "تاریخ انعقاد قرارداد": data.contract_date,
        "کاربری": data.use_type,
        "آورده شهرداری": data.municipality_description,
        "شناسه ملی سرمایه گذار": data.contractor_national_code,
        "شناسه اقتصادی سرمایه‌گذار": data.contractor_economic_code,
        "آورده‌ی سرمایه گذار": data.investor_description,
        "برآورد زمان اتمام/بهره برداری پروژه": data.contract_duration,
        "مدل قرارداد مشارکت": data.contract_type,
        "آدرس محل انجام کار": data.address,
        "تاریخ شروع": data.start_date,
        "تاریخ پایان": data.end_date,
        "امضا کننده‌ی قرارداد از طرف کارفرما ": data.contractor_signer,
        "نام مدیرعامل": data.manager_name,
        " ناظر قرارداد": data.supervisor_name,
        "متولی": data.trustee_name
    } 
    for data in datas
    ]
    df = pd.DataFrame(data=excel_data)
    df.to_excel("قراردادهای مشارکتی.xlsx")
    response = FileResponse(open('قراردادهای مشارکتی.xlsx', 'rb'), as_attachment=True, filename='قراردادهای مشارکتی.xlsx')
    return response

def AllParticipatoryData(request):
    participatorydatas = ParticipatoryData.objects.all()
    result = []
    for participatorydata in participatorydatas:
        temp = {}
        temp["موضوع قرارداد"]= participatorydata.contract_subject
        temp["ذینفع"]= participatorydata.beneficiary_name
        temp["سرمایه گذار"]= participatorydata.contractor_name
        temp["نام پروژه"]= participatorydata.project_subject
        temp["مبلغ نهایی قرارداد"]= participatorydata.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(participatorydata.contract_date)
        temp["کاربری"]= participatorydata.use_type
        temp["آورده شهرداری"]= participatorydata.municipality_description
        temp["شناسه ملی سرمایه گذار"]= participatorydata.contractor_national_code
        temp["شناسه اقتصادی سرمایه‌گذار"]= participatorydata.contractor_economic_code
        temp["آورده‌ی سرمایه گذار"]= participatorydata.investor_description
        temp["برآورد زمان اتمام/بهره برداری پروژه"]= participatorydata.contract_duration
        temp["مدل قرارداد مشارکت"]= participatorydata.contract_type
        temp["آدرس محل انجام کار"]= participatorydata.address
        temp["تاریخ شروع"]= str(participatorydata.start_date)
        temp["تاریخ پایان"]= str(participatorydata.end_date)
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= participatorydata.contractor_signer
        temp["نام مدیرعامل"]= participatorydata.manager_name
        temp[" ناظر قرارداد"]= participatorydata.supervisor_name
        temp["متولی"]= participatorydata.trustee_name
        result.append(temp)
    return JsonResponse({'isOk': True, 'result': result})

def AllCostData(request):
    cost_datas = Data.objects.filter(type="C")
    result = []
    for cost_data in cost_datas:
        temp = {}
        temp["موضوع قرارداد"]= cost_data.contract_subject
        temp["نام پیمانکار"]= cost_data.contractor_name
        temp["نام کارفرما"]= cost_data.employer_name
        temp["مبلغ نهایی قرارداد"]= cost_data.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(cost_data.contract_date)
        temp["نام مدیرعامل"]= cost_data.manager_name
        temp["مبلغ غیرنقد"]= cost_data.non_cash
        temp["مبلغ نقد"]= cost_data.cash
        temp[" اطلاعات فسخ قرارداد"]= cost_data.contract_termination_information
        temp[" اطلاعات الحاقیه"]= cost_data.contract_attachmentes
        temp["مدت قرارداد"]= cost_data.contract_duration
        temp["روش انتخاب پیمانکار"]= cost_data.contractor_selection
        temp["شناسه ملی پیمانکار"]= cost_data.contractor_national_code
        temp["کد اقتصادی پیمانکار"]= cost_data.contractor_economic_code
        temp["تاریخ تولد پیمانکار"]= str(cost_data.contractor_birthdate)
        temp["علت ترک تشریفات"]= cost_data.leave_reason
        temp["مالیات  بر ارزش افزوده دارد؟"]= cost_data.VAT
        temp["تاریخ شروع"]= str(cost_data.start_date)
        temp["تاریخ پایان"]= str(cost_data.end_date)
        temp[" معاون"]= cost_data.supervisor_name
        temp["روش تعیین برنده"]= cost_data.winniner_determination_method
        temp["اعضای کمیسیون"]= cost_data.commisstion_member
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= cost_data.contractor_signer
        result.append(temp)
    return JsonResponse({'isOk': True, 'result': result})

def AllIncomeData(request):
    income_datas = Data.objects.filter(type="I")
    result = []
    for income_data in income_datas:
        temp = {}
        temp["موضوع قرارداد"]= income_data.contract_subject
        temp["نام پیمانکار"]= income_data.contractor_name
        temp["نام کارفرما"]= income_data.employer_name
        temp["مبلغ نهایی قرارداد"]= income_data.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(income_data.contract_date)
        temp["نام مدیرعامل"]= income_data.manager_name
        temp["مبلغ غیرنقد"]= income_data.non_cash
        temp["مبلغ نقد"]= income_data.cash
        temp[" اطلاعات فسخ قرارداد"]= income_data.contract_termination_information
        temp[" اطلاعات الحاقیه"]= income_data.contract_attachmentes
        temp["مدت قرارداد"]= income_data.contract_duration
        temp["روش انتخاب پیمانکار"]= income_data.contractor_selection
        temp["شناسه ملی پیمانکار"]= income_data.contractor_national_code
        temp["کد اقتصادی پیمانکار"]= income_data.contractor_economic_code
        temp["تاریخ تولد پیمانکار"]= str(income_data.contractor_birthdate)
        temp["علت ترک تشریفات"]= income_data.leave_reason
        temp["مالیات  بر ارزش افزوده دارد؟"]= income_data.VAT
        temp["تاریخ شروع"]= str(income_data.start_date)
        temp["تاریخ پایان"]= str(income_data.end_date)
        temp[" معاون"]= income_data.supervisor_name
        temp["روش تعیین برنده"]= income_data.winniner_determination_method
        temp["اعضای کمیسیون"]= income_data.commisstion_member
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= income_data.contractor_signer
        result.append(temp)
        
    return JsonResponse({'isOk': True, 'result': result})


def FilterParticipatoryData(request):
    try:
        filter = {} 
        filter['address'] = request.POST.get('آدرس محل انجام کار')
        filter['contractor_name'] = request.POST.get('سرمایه گذار')
        filter['contract_price__gte'] = request.POST.get('مبلغ قرارداد از')
        filter['contract_price__lte'] = request.POST.get('تا')
        filter['contract_date__gte'] = request.POST.get('از تاریخ')
        filter['contract_date__lte'] = request.POST.get('تا تاریخ')
        new_filter = {k:v for k,v in filter.items() if v}
        participatorydatas = ParticipatoryData.objects.filter(**new_filter)
        
    except Exception as e:
         return JsonResponse({'isOk': False, 'error': "Enterd data is not valid"})
        
    result = []
    for participatorydata in participatorydatas:
        temp = {}
        temp["موضوع قرارداد"]= participatorydata.contract_subject
        temp["ذینفع"]= participatorydata.beneficiary_name
        temp["سرمایه گذار"]= participatorydata.contractor_name
        temp["نام پروژه"]= participatorydata.project_subject
        temp["مبلغ نهایی قرارداد"]= participatorydata.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(participatorydata.contract_date)
        temp["کاربری"]= participatorydata.use_type
        temp["آورده شهرداری"]= participatorydata.municipality_description
        temp["شناسه ملی سرمایه گذار"]= participatorydata.contractor_national_code
        temp["شناسه اقتصادی سرمایه‌گذار"]= participatorydata.contractor_economic_code
        temp["آورده‌ی سرمایه گذار"]= participatorydata.investor_description
        temp["برآورد زمان اتمام/بهره برداری پروژه"]= participatorydata.contract_duration
        temp["مدل قرارداد مشارکت"]= participatorydata.contract_type
        temp["آدرس محل انجام کار"]= participatorydata.address
        temp["تاریخ شروع"]= str(participatorydata.start_date)
        temp["تاریخ پایان"]= str(participatorydata.end_date)
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= participatorydata.contractor_signer
        temp["نام مدیرعامل"]= participatorydata.manager_name
        temp[" ناظر قرارداد"]= participatorydata.supervisor_name
        temp["متولی"]= participatorydata.trustee_name
        result.append(temp)
    
    return JsonResponse({'isOk': True, 'result': result})
        

def FilterCostData(request):
    try:
        filter = {}
        filter['type'] = "C"
        filter['employer_name']= request.POST.get('نام کارفرما')
        filter['contractor_name']= request.POST.get('نام پیمانکار')
        filter['contract_price__gte']= request.POST.get('از مبلغ')
        filter['contract_price__lte']= request.POST.get('تا مبلغ')
        filter['contract_date__gte']= request.POST.get('از تاریخ')
        filter['contract_date__lte']= request.POST.get('تا تاریخ')

        new_filter = {k:v for k,v in filter.items() if v}      
        cost_datas = Data.objects.filter(**new_filter)
    except Exception as e:
        return JsonResponse({'isOk': False, 'error': "Enterd data is not valid"})
        
    result = []
    for cost_data in cost_datas:
        temp = {}
        temp["موضوع قرارداد"]= cost_data.contract_subject
        temp["نام پیمانکار"]= cost_data.contractor_name
        temp["نام کارفرما"]= cost_data.employer_name
        temp["مبلغ نهایی قرارداد"]= cost_data.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(cost_data.contract_date)
        temp["نام مدیرعامل"]= cost_data.manager_name
        temp["مبلغ غیرنقد"]= cost_data.non_cash
        temp["مبلغ نقد"]= cost_data.cash
        temp[" اطلاعات فسخ قرارداد"]= cost_data.contract_termination_information
        temp[" اطلاعات الحاقیه"]= cost_data.contract_attachmentes
        temp["مدت قرارداد"]= cost_data.contract_duration
        temp["روش انتخاب پیمانکار"]= cost_data.contractor_selection
        temp["شناسه ملی پیمانکار"]= cost_data.contractor_national_code
        temp["کد اقتصادی پیمانکار"]= cost_data.contractor_economic_code
        temp["تاریخ تولد پیمانکار"]= str(cost_data.contractor_birthdate)
        temp["علت ترک تشریفات"]= cost_data.leave_reason
        temp["مالیات  بر ارزش افزوده دارد؟"]= cost_data.VAT
        temp["تاریخ شروع"]= str(cost_data.start_date)
        temp["تاریخ پایان"]= str(cost_data.end_date)
        temp[" معاون"]= cost_data.supervisor_name
        temp["روش تعیین برنده"]= cost_data.winniner_determination_method
        temp["اعضای کمیسیون"]= cost_data.commisstion_member
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= cost_data.contractor_signer
        result.append(temp)

    return JsonResponse({'isOk': True, 'result': result})
        

def FilterIncomeData(request):
    try:
        filter = {}
        filter['type'] = "I"
        filter['employer_name']= request.POST.get('نام کارفرما')
        filter['contractor_name']= request.POST.get(' نام پیمانکار')
        filter['contract_price__gte']= request.POST.get('از مبلغ')
        filter['contract_price__lte']= request.POST.get('تامبلغ')
        filter['contract_date__gte']= request.POST.get('از تاریخ')
        filter['contract_date__lte']= request.POST.get('تا تاریخ')
        new_filter = {k:v for k,v in filter.items() if v}
        income_datas = Data.objects.filter(**new_filter)
    except Exception as e:
        return JsonResponse({'isOk': True, 'error': "Enterd data is not valid"})

    result = []
    for income_data in income_datas:
        temp = {}
        temp["موضوع قرارداد"]= income_data.contract_subject
        temp["نام پیمانکار"]= income_data.contractor_name
        temp["نام کارفرما"]= income_data.employer_name
        temp["مبلغ نهایی قرارداد"]= income_data.contract_price
        temp["تاریخ انعقاد قرارداد"]= str(income_data.contract_date)
        temp["نام مدیرعامل"]= income_data.manager_name
        temp["مبلغ غیرنقد"]= income_data.non_cash
        temp["مبلغ نقد"]= income_data.cash
        temp[" اطلاعات فسخ قرارداد"]= income_data.contract_termination_information
        temp[" اطلاعات الحاقیه"]= income_data.contract_attachmentes
        temp["مدت قرارداد"]= income_data.contract_duration
        temp["روش انتخاب پیمانکار"]= income_data.contractor_selection
        temp["شناسه ملی پیمانکار"]= income_data.contractor_national_code
        temp["کد اقتصادی پیمانکار"]= income_data.contractor_economic_code
        temp["تاریخ تولد پیمانکار"]= str(income_data.contractor_birthdate)
        temp["علت ترک تشریفات"]= income_data.leave_reason
        temp["مالیات  بر ارزش افزوده دارد؟"]= income_data.VAT
        temp["تاریخ شروع"]= str(income_data.start_date)
        temp["تاریخ پایان"]= str(income_data.end_date)
        temp[" معاون"]= income_data.supervisor_name
        temp["روش تعیین برنده"]= income_data.winniner_determination_method
        temp["اعضای کمیسیون"]= income_data.commisstion_member
        temp["امضا کننده‌ی قرارداد از طرف کارفرما "]= income_data.contractor_signer
        result.append(temp)
        
    return JsonResponse({'isOk': True, 'result': result})