from __future__ import absolute_import, unicode_literals

import os

from celery.schedules import crontab
from celery           import Celery
from datetime         import timedelta

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'shafaf_tehran.settings')

celery_app = Celery('shafaf_tehran')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()
celery_app.conf.beat_schedule = {
    'GetData': {
        'task': 'api.views.GetData',
        'schedule': crontab(hour=0, minute=0, day_of_week=1),
    },
}
