# Generated by Django 2.2.10 on 2020-04-02 07:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20200402_0647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='costdata',
            name='cash',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='contranct_price',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='costdata',
            name='non_cash',
            field=models.BigIntegerField(null=True),
        ),
    ]
