from django.db                   import models
from django_jalali.db            import models as jmodels

class Data(models.Model):

    contract_termination_information = models.TextField(null = True)
    winniner_determination_method     = models.CharField(max_length = 255, null=True)
    contractor_national_code = models.BigIntegerField(null=True)
    contractor_economic_code = models.BigIntegerField(null=True)
    contractor_birthdate  = jmodels.jDateField(null=True)
    contract_attachmentes = models.TextField(null=True)
    contractor_selection  = models.TextField(null=True)
    commisstion_member    = models.TextField(null=True)
    contractor_signer     = models.CharField(max_length = 255, null=True)
    contract_subject = models.CharField(max_length = 255, null=True)
    contractor_name  = models.CharField(max_length = 255, null=True)
    employer_name    = models.CharField(max_length = 255, null=True)
    manager_name     = models.CharField(max_length = 255, null=True)
    leave_reason     = models.TextField(null = True)
    supervisor_name  = models.CharField(max_length = 255, null=True)
    contract_duration = models.CharField(max_length = 255, null=True)
    contract_price    = models.BigIntegerField(null=True)
    national_code     = models.BigIntegerField(null=True)
    contract_date     = jmodels.jDateField(null=True)
    start_date = jmodels.jDateField(null=True)
    end_date   = jmodels.jDateField(null=True)
    non_cash   = models.BigIntegerField(null=True)
    cash       = models.BigIntegerField(null=True)
    VAT        = models.BooleanField(null=True)
    TYPE_CHOICE = (
		('C', 'Cost'),
		('I', 'Income')
	)
    type = models.CharField(
        max_length = 1,
        choices = TYPE_CHOICE

    )

    def __str__(self):
        return self.contract_subject

class ParticipatoryData(models.Model):
    
    municipality_description = models.CharField(max_length = 255, null=True) 
    investor_description     = models.CharField(max_length = 255, null=True)
    contractor_signer        = models.CharField(max_length = 255, null=True)
    contractor_national_code = models.BigIntegerField(null=True)
    contractor_economic_code = models.BigIntegerField(null=True) 
    contract_duration = models.CharField(max_length = 255, null=True)
    contract_subject  = models.CharField(max_length = 255, null=True)
    beneficiary_name  = models.CharField(max_length = 255, null=True) 
    project_subject   = models.CharField(max_length = 255, null=True)
    manager_name      = models.CharField(max_length = 255, null=True) 
    supervisor_name   = models.CharField(max_length = 255, null=True)
    contract_type     = models.CharField(max_length = 255, null=True) 
    contractor_name   = models.CharField(max_length = 255, null=True) 
    use_type          = models.CharField(max_length = 255, null=True) 
    trustee_name      = models.CharField(max_length = 255, null=True)
    contract_price    = models.BigIntegerField(null=True) 
    contract_date     = jmodels.jDateField(null=True) 
    start_date = jmodels.jDateField(null=True)
    end_date   = jmodels.jDateField(null=True)
    address    = models.TextField(null = True)

    def __str__(self):
        return self.contract_subject







    

